'use strict';

let autoPark = [];
let autoObj = {};

let indexElementa;

let ispravnaTablica1 = /[A-Z,Đ,Š,Ž,Ć,Č]{2}-[0-9]{3,4}-[A-Z,Đ,Š,Ž,Ć,Č]{2}$/;
let ispravnaTablica2 = /[A-Z,Đ,Š,Ž,Ć,Č]{2}-[0-9,A-Z,Đ,Š,Ž,Ć,Č]{3,5}$/;

let ispravnoGodiste1 = /[1][8][8][4-9]$/;
let ispravnoGodiste2 = /[1][8][9][0-9]$/;
let ispravnoGodiste3 = /[1][9][0-9]{2}$/;
let ispravnoGodiste4 = /[2][0][0,1][0-9]$/;
let ispravnoGodiste5 = /[2][0][2][0-2]$/;

let tab1 = document.querySelector('#tabela1');
let tab2 = document.querySelector('#tabela2');
let tab3 = document.querySelector('#tabela3');

const tablica = document.getElementById('tablica');
const marka = document.getElementById('marka');
const godiste = document.getElementById('godiste');
const kilometraza = document.getElementById('kilometraza');

//elementi od modala
let tablicaEdit = document.querySelector('#tablicaEdit');
let markaEdit = document.querySelector('#markaEdit');
let godisteEdit = document.querySelector('#godisteEdit');
let kilometrazaEdit = document.querySelector('#kilometrazaEdit');
let izmeniButton = document.querySelector('#izmeniObjekat');

document.querySelector("#napraviObjekat").addEventListener('click', napraviNiz);

function napraviNiz() {
    let ispravnaTablica = (ispravnaTablica1.test(tablica.value) || ispravnaTablica2.test(tablica.value));
    let uslovGodiste = (
        ispravnoGodiste1.test(godiste.value) ||
        ispravnoGodiste2.test(godiste.value) ||
        ispravnoGodiste3.test(godiste.value) ||
        ispravnoGodiste4.test(godiste.value) ||
        ispravnoGodiste5.test(godiste.value)
    );
    if (tablica.value == "" || marka.value == "" || godiste.value == "" || kilometraza.value == "") {
        alert("Sva polja MORAJU biti popunjena!");
    }
    if (tablica.value !== "" && marka.value !== "" && godiste.value !== "" && kilometraza.value !== "") {
        var vozniParkObj = {
            registarska_tablica: tablica.value,
            marka: marka.value,
            godiste: godiste.value,
            kilometraza: kilometraza.value
        }
        autoObj = vozniParkObj;
        let duplikatTablice = autoPark.some(e => e.registarska_tablica === autoObj.registarska_tablica);
        if (duplikatTablice) {
            return alert(`U voznom parku vec POSTOJI automobil sa registarskom oznakom koju ste upravo uneli!`);
        }
        if (!ispravnaTablica){
            return alert(`Morate uneti ispravan format registarske oznake!`);
        }
        if (!uslovGodiste){
            return alert(`Morate uneti ispravan format godine!`);
        }
        if (kilometraza.value<0){
            return alert(`Unos za kilometrazu ne moze da bude negativan broj!`);
        }
        else {
            autoPark.push(vozniParkObj);
            tablica.value = "";
            marka.value = "";
            godiste.value = "";
            kilometraza.value = "";
            showArray();
        }
    }
}

function deleteItem(arr, index) {
    let questionDelete = confirm(`Da li ste zaista sigurni da zelite da izbrisete automobil registarske oznake ${autoPark[index].registarska_tablica} iz voznog parka?!`);
    if(questionDelete){
        arr.splice(index, 1);
        showArray();
    }
}

function showArray() {
    let currentYear = new Date().getFullYear();
    tab1.innerHTML = '';
    tab2.innerHTML = '';
    tab3.innerHTML = '';
    for (let i = 0; i < autoPark.length; i++) {
        let auto = autoPark[i];
        let starostVozila = currentYear - Number(auto.godiste);
        if(starostVozila < 10 || auto.kilometraza < 150000){
            tableGenerator(tab1, i, auto);
        }
        if(starostVozila >= 10 && starostVozila <= 20 || auto.kilometraza >= 150000 && auto.kilometraza <= 300000){
            tableGenerator(tab2, i, auto);
        }
        if(starostVozila > 20 || auto.kilometraza > 300000){
            tableGenerator(tab3, i, auto);
        }
    }
};

function tableGenerator(table, i, auto){
    table.innerHTML += `
    <tr>
    <td>${auto.registarska_tablica}</td>
    <td>${auto.marka}</td>
    <td>${auto.godiste}</td>
    <td>${auto.kilometraza}</td>
    <td><button onclick="openModal(${i}); indexElementa = ${i};">Izmeni</button></td>
    <td><button onclick="deleteItem(autoPark, ${i});">Obrisi</button></td>
    </tr>
    `
};
//KOD ZA MODAL
const modal = document.querySelector('.modal');
const overlay = document.querySelector('.overlay');
const btnCloseModal = document.querySelector('.close-modal');
const btnsOpenModal = document.querySelectorAll('.show-modal');

function openModal(index) {
    tablicaEdit.innerHTML = autoPark[index].registarska_tablica;
    markaEdit.value = autoPark[index].marka;
    godisteEdit.value = autoPark[index].godiste;
    kilometrazaEdit.value = autoPark[index].kilometraza;
    modal.classList.remove('hidden');
    overlay.classList.remove('hidden');

};
izmeniButton.addEventListener('click', function(){
    editedEntry(indexElementa);
});

//nesto izmena,,,,
//dsafdas

function editedEntry(indexElementa){
    let index = indexElementa;
    let uslovGodiste = (
        ispravnoGodiste1.test(godisteEdit.value) ||
        ispravnoGodiste2.test(godisteEdit.value) ||
        ispravnoGodiste3.test(godisteEdit.value) ||
        ispravnoGodiste4.test(godisteEdit.value) ||
        ispravnoGodiste5.test(godisteEdit.value)
    );
    if (tablicaEdit.value == "" || markaEdit.value == "" || godisteEdit.value == "" || kilometrazaEdit.value == "") {
        alert("Sva polja MORAJU biti popunjena!");
    }
    if (tablicaEdit.value !== "" && markaEdit.value !== "" && godisteEdit.value !== "" && kilometrazaEdit.value !== ""){
        if(!uslovGodiste){
            return alert(`Morate uneti ispravan format godine!`);
        }
        else{
                tablicaEdit.innerHTML = autoPark[index].registarska_tablica;
                autoPark[index].marka = markaEdit.value;
                autoPark[index].godiste = godisteEdit.value;
                autoPark[index].kilometraza = kilometrazaEdit.value;
                closeModal();
                showArray();
        }
    }
};

const closeModal = function () {
    modal.classList.add('hidden');
    overlay.classList.add('hidden');
};

for (let i = 0; i < btnsOpenModal.length; i++) 
    btnsOpenModal[i].addEventListener('click', openModal);
    btnCloseModal.addEventListener('click', closeModal);
    overlay.addEventListener('click', closeModal);

    document.addEventListener('keydown', function (e){
        if (e.key === 'Escape' && !modal.classList.contains('hidden')) {
            closeModal();
        }
    }
);